import onnx
import argparse
import graphviz
import sys


class ShowPrinter(object):
    """The ShowPrinter class allows to create an object which can parse an
    ONNX model file, then the model can be printed to the terminal or
    an svg file"""

    def __init__(self, input, output):
        """
        :param input: input ONNX file
        :param output: (optional) output SVG file
        """
        self.input_file = input
        self.output_file = output
        self.model = None

    def parse_file(self):
        """
        Initiate parsing of the ONNX file.
        This method must be called before ``print_term`` or ``print_svg``
        :return:
        """
        self.model = onnx.load_model(self.input_file)

    def print_term(self):
        """
        Print all nodes of the incoming ONNX graph in a sequential manner to
        the terminal output

        :return: None
        """
        for node in self.model.graph.node:
            print(node.name)

    def print_svg(self):
        """
        Produces two files: ``DiGraph.gv`` and ``DiGraph.gv.svg``
        This is a visual representation of your graph.

        :return: None
        """
        # Create a directed graph
        graph = graphviz.Digraph()
        # Go over all of the graph's nodes
        for node in self.model.graph.node:
            graph.node(node.name)
            # Connect all the nodes inputs to this graph
            for input_node in node.input:
                graph.node(input_node)
                # Add directed edge (input) -> (node)
                graph.edge(input_node, node.name)
            for output_node in node.output:
                # Add directed edge (node) -> (output)
                graph.edge(node.name, output_node)
        graph.format = 'svg'
        graph.render(directory='.')


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Display and analyze ONNX graphs")
    parser.add_argument('input_file', type=str, help='Input ONNX file')
    parser.add_argument('--output_file', type=str, help="Output SVG file",
                        default=None)

    # If no arguments were supplied, show help message.
    if len(sys.argv) < 2:
        parser.print_help()
        sys.exit(1)

    args = parser.parse_args()
    # currently this functionality is not implemented yet.
    if args.output_file is not None:
        raise NotImplementedError("Output file not yet implemented")
    show_printer = ShowPrinter(args.input_file, args.output_file)
    try:
        show_printer.parse_file()
    except FileNotFoundError:
        print("No model file {} found".format(args.input_file))
        exit(1)
    show_printer.print_term()
    show_printer.print_svg()
