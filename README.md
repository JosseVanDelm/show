# Show

Python tool to parse and display ONNX graphs

 [![pipeline status](https://gitlab.com/JosseVanDelm/show/badges/main/pipeline.svg)](https://gitlab.com/JosseVanDelm/show/-/commits/main)
 [![License: WTFPL](https://img.shields.io/badge/License-WTFPL-brightgreen.svg)](http://www.wtfpl.net/about/)

## Documentation

Documentation is hosted on [GitLab Pages](https://jossevandelm.gitlab.io/show/):
* [Installation](https://jossevandelm.gitlab.io/show/installation.html)
* [Usage](https://jossevandelm.gitlab.io/show/usage.html)
* [Contribution guidelines](https://jossevandelm.gitlab.io/show/contribute.html)

## Notes

*This tool is currently under heavy development*