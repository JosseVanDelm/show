==========================
Contribute to this project
==========================

Creating a merge request
========================
This project is hosted on `GitLab <https://gitlab.com/JosseVanDelm/show>`_.
To contribute to this project:

1. Fork this project on GitLab.
2. Create a merge request to push your local changes to our repository.
3. Our CI will go through your code and test it.
4. Our reviewers will review your code and if it meets our standards, we will merge your code.

Contributing guidelines
=======================

We require all python code related to this project to be pushed in the ``show/`` folder.
Our CI will test and make sure of the following:

* If your code does not pass all integration tests, your code will not be merged.
* If your code does not conform to `PEP8 <https://www.python.org/dev/peps/pep-0008/>`_, it will not be pushed to the main branch.
  This is enforced by our CI.

