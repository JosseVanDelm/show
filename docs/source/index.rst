.. Show documentation master file, created by
   sphinx-quickstart on Fri Nov 26 17:27:42 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

================================
Welcome to Show's documentation!
================================

Show is a simple tool for visualizing ONNX neural network graphs.

.. warning::
   This tool is currently under heavy development and its functionality is subject to change.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation.rst
   usage.rst
   contribute.rst
   reference.rst



Search this documentation
=========================

* :ref:`search`
* :ref:`modindex`
