# Show Documentation

Show's documentation is built with `sphinx` which is a python documentation generation tool.
The latest documentation build is also hosted on [Gitlab Pages](https://jossevandelm.gitlab.io/show).

Installation  and usage instructions for locally building the documentation are included below.
## Local Installation
The python requirements for documentation are listed in `requirements.txt` in this directory.
From within the `/docs` folder execute:
```sh
pip install -r requirements.txt
```
## Local Documentation Build
From within the `/docs` folder execute:
```sh
make html
```